package com.example.testapi.models.mappers;

import com.example.testapi.models.domain.Guitar;
import com.example.testapi.models.dto.GuitarBrandsDto;

import java.util.Map;

import java.util.stream.Collectors;

public class GuitarBrandsMapper {

    public static GuitarBrandsDto mapGuitarBrandsDto(Map<Integer, Guitar> guitars) {
        //Creates a new map from guitars, streams, collects, and groups the values by key and value
        //In this case brand and then the result of counting those elements.
        //Returns the data to the DTO, and through to the request.
        var bar = guitars
                .values()
                .stream()
                .collect(Collectors.groupingBy(Guitar::getBrand, Collectors.counting()));
        return new GuitarBrandsDto(bar);
    }

}


package com.example.testapi.models.dto;

import java.util.Map;

//DTO, for getting the numb of guitars per brand as a new map.
//With String as brand, and Long as sum of guitars per brand.
public class GuitarBrandsDto {
    private Map<String, Long> numOfEachBrand;

    public Map<String, Long> getNumOfEachBrand() {
        return numOfEachBrand;
    }

    public void setNumOfEachBrand(Map<String, Long> numOfEachBrand) {
        this.numOfEachBrand = numOfEachBrand;
    }

    public GuitarBrandsDto(Map<String, Long> numOfEachBrand) {
        this.numOfEachBrand = numOfEachBrand;
    }
}

package com.example.testapi.dataaccess;
import com.example.testapi.models.domain.Guitar;
import com.example.testapi.models.dto.GuitarBrandsDto;

import com.example.testapi.models.mappers.GuitarBrandsMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component //Marks the repos as a BEAN = packaged code to be used.
public class GuitarRepository implements IGuitarRepository {

    private Map<Integer, Guitar> guitars = seedGuitars();

    //Generates a HashMap with two guitar objects and their values.
    private Map<Integer, Guitar> seedGuitars() {
        var guitars = new HashMap<Integer, Guitar>();

        guitars.put(1, new Guitar("Fender", "Telecaster"));
        guitars.put(2, new Guitar("Jackson", "Kelly"));

        return guitars;
    }

    //Gets the entire guitars hashmap.
    @Override
    public Map<Integer, Guitar> getAllGuitars() {
        return guitars;

    }

    //Gets a specific guitar id from guitars hash.
    @Override
    public Guitar getGuitar(int id) {
        return guitars.get(id);
    }

    //Adds a guitar, checks the current max key and increments it with one
    //with the new guitar that is created. This was a finding by Paulo!
    @Override
    public Guitar addGuitar(Guitar guitar) {
        Integer maxId = Collections.max(guitars.keySet());
        guitars.put(maxId+1, guitar);
        return guitars.get(maxId+1);
    }

    //Replaces a guitar with a new object/guitar.
    @Override
    public Guitar replaceGuitar(int id, Guitar guitar) {
        guitars.replace(id, guitar);
        return guitars.get(id);


    }

    //Modify target guitar.
    @Override
    public Guitar modifyGuitar(int id, Guitar guitar) {
        var guitarToModify = getGuitar(id);

        guitarToModify.setBrand(guitar.getBrand());
        guitarToModify.setModel(guitar.getModel());

        return guitarToModify;
    }

    //Removes guitars with target id.
    @Override
    public void deleteGuitar(int id) {
        guitars.remove(id);
    }


    //Check if targeted guitar id exists.
    @Override
    public boolean guitarExists(int id) {
        if (getGuitar(id) == null) {
            return false;
        }
        return true;
    }


    //Check if the structure of the data is OK or NOK.
    @Override
    public boolean isValidGuitar(Guitar guitar) {
       return guitar.getBrand() != null && guitar.getModel() != null;
    }

    //Get numbs of guitars for each brand through mapGuitarBrandsDto.
    @Override
    public GuitarBrandsDto getNumForEachBrand() {

        return GuitarBrandsMapper.mapGuitarBrandsDto(guitars);
    }


}


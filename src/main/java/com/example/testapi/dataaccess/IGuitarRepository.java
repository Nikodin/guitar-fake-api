package com.example.testapi.dataaccess;

import com.example.testapi.models.domain.Guitar;
import com.example.testapi.models.dto.GuitarBrandsDto;

import java.util.Map;

public interface IGuitarRepository {
    Map<Integer, Guitar> getAllGuitars();
    Guitar getGuitar(int id);
    Guitar addGuitar(Guitar guitar);
    Guitar replaceGuitar(int id, Guitar guitar);
    Guitar modifyGuitar(int id, Guitar guitar);
    void deleteGuitar(int id);
    boolean guitarExists(int id);
    boolean isValidGuitar (Guitar guitar);
    GuitarBrandsDto getNumForEachBrand();
}

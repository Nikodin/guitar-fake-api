package com.example.testapi.controllers;

import com.example.testapi.dataaccess.IGuitarRepository;
import com.example.testapi.models.domain.Guitar;
import com.example.testapi.models.dto.GuitarBrandsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/guitars") //First takes the base, not needed to repeat everytime!.
public class GuitarController {
    //Allows the use of the repos, as if you'd new up.
    @Autowired
    private IGuitarRepository guitarRepository;

    //Get all the objects within the map.
   @GetMapping
    public ResponseEntity<Map<Integer, Guitar>> getAllGuitars(){
        return new ResponseEntity<>(guitarRepository.getAllGuitars(), HttpStatus.OK);
    }

    //Get the targeted id through url. Checks if object exits through guitarExists,
    //if not = error. If true, returns targeted guitar.
    @GetMapping("/{id}")
    public ResponseEntity<Guitar> getGuitar(@PathVariable int id) {
       if (!guitarRepository.guitarExists(id)) {
           return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.NOT_FOUND);
       }
       return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.OK);
    }

    //Creates a guitar through method, if body does not match the structure, "Conflict"
    //If no error, and input is correct, guitar is created.
    //Only saved in memory, volatile!
    @PostMapping
    public ResponseEntity<Guitar>createCreateGuitar(@RequestBody Guitar guitar) {
        if (!guitarRepository.isValidGuitar(guitar))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);{

        }
        return new ResponseEntity<>(guitarRepository.addGuitar(guitar), HttpStatus.CREATED);
   }


    //Tries to replace target guitar object, if replacement is not correct in structure - bad request
    //If Guitar does not exist, state object is not found. If correct, replace.
    //An improvement could be to add a guitar if nothing is there?
    @PutMapping("/{id}")
    public ResponseEntity<Guitar>replaceGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
       if (!guitarRepository.isValidGuitar(guitar)) {
           return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
       }
                if (!guitarRepository.guitarExists(id)){
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);

                }

               return new ResponseEntity<>(guitarRepository.replaceGuitar(id, guitar), HttpStatus.NO_CONTENT);

    }

    //Patches targeted id, if guitar does not exist = not found. [SCRAPPED: If not valid structure = Conflict.]
    //If no errors, modifies targeted guitar.
    @PatchMapping("/{id}")
    public ResponseEntity<Guitar>modifyGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
       if(!guitarRepository.guitarExists(id)) {
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
//       if (!guitarRepository.isValidGuitar(guitar)){
//            return new ResponseEntity<>(HttpStatus.CONFLICT)
//       } -- This would wreck new additions to the object.... scrapped.
       return new ResponseEntity<>(guitarRepository.modifyGuitar(id, guitar), HttpStatus.OK);
    }


    //Deletes targeted guitar id, if it doesn't exist = not found.
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteGuitar(@PathVariable int id) {
       if(!guitarRepository.guitarExists(id)){
           return new ResponseEntity<>("Does not exist", HttpStatus.NOT_FOUND);
       }
       guitarRepository.deleteGuitar(id);
       return new ResponseEntity<>("Guitar with id " + id + " has been removed!", HttpStatus.OK);
    }

    //Gets the total number of guitars per brand as a new map through the DTO and mapper created.
    @GetMapping("/brands")
    public ResponseEntity<GuitarBrandsDto>NumOfGuitarPerBrand()  {
        return new ResponseEntity<>(guitarRepository.getNumForEachBrand(), HttpStatus.OK);
    }

}


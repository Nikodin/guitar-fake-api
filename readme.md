# Guitar Fake API

### How it works:

A Spring Boot App with an example "database" in-built when booted  
a hashmap of two "guitars", each with a brand and model name.  
  
The app simulates the usage of controller methods, through the   
Postman application. The project boots locally. 

http://localhost:8080/api/v1/guitars - Is the main page for this app.

Through Postman we can use these requests as JSON, structure must be matched for 
edits to be accepted. If not, you will get an error. 


**GET Request the full hashmap of guitars:**  
  http://localhost:8080/api/v1/guitars  
*Gets the current hashmap of guitars*

**GET Request a specific guitar by id:**  
  http://localhost:8080/api/v1/guitars/ (insert id number here)  
*Gets the specific entered id through url, enter a number for the specific guitar*

**POST Create a guitar:**  
http://localhost:8080/api/v1/guitars  
*Create a guitar with the same structure as the examples, structure must match. An ID is auto-generated.*
*See the first GET request for examples...*


**PUT Replace a guitar:**  
http://localhost:8080/api/v1/guitars/ (targeted id number to replace)  
*Targets the url id to replace, finds the guitar and replaces it with the new values,  
structure must match the examples to be accepted.*

**PATCH Modify guitar:**  
http://localhost:8080/api/v1/guitars/ (Insert id number here) 
*Targets the entered url id to be modified, values must match the pattern of examples to be accepted.*

**DELETE guitar:**  
http://localhost:8080/api/v1/guitars/ (Insert target id here)  
*Deletes targeted id through url, if it exists.*

**GET Request number of guitars per brand:**  
http://localhost:8080/api/v1/guitars/brands 
*Gets the current list of "Brands" and their respective amount of guitars*

*Check the code for comments/details!*

### About
An assignment, simulating controller functions, annotations, DTO's, and requests. All through 
a fake-api.  

Very hard for some (me) but nevertheless interesting. 


